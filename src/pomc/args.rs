//extern crate structopt;

use std::str::FromStr;
use structopt::StructOpt;

#[derive(Debug)]
pub enum Action {
    Play,
    Pause,
    Stop,
    Status,
    Toggle,
}

impl FromStr for Action {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let lower = s.to_lowercase();
        match lower.as_str() {
            "play" => Ok(Action::Play),
            "pause" => Ok(Action::Pause),
            "stop" => Ok(Action::Stop),
            "status" => Ok(Action::Status),
            "toggle" => Ok(Action::Toggle),
            _ => Err("Unknown format"),
        }
    }
}

#[derive(StructOpt, Debug)]
#[structopt(
    name = "pomc",
    setting = structopt::clap::AppSettings::ColoredHelp
)]
pub struct Opt {
    ///Action to send to pomd.
    #[structopt(
        name = "ACTION",
        possible_values = &["play", "pause", "stop", "toggle", "status"]
    )]
    pub action: Action,
    /// Get the status in json format.
    #[structopt(short, long)]
    pub(crate) json: bool,
}

pub fn get_args() -> Opt {
    Opt::from_args()
}
