mod args;

use args::{get_args, Action};
use common::{create_socket, error::PomdError, socket_path};
use std::process;
use std::str;

fn main() -> Result<(), PomdError> {
    let server = socket_path("pomd");

    let socket = match create_socket(&socket_path("pomc")) {
        Ok(s) => s,
        Err(e) => {
            eprintln!("Failed to create the Unix socket: {}", e);
            process::exit(1);
        }
    };

    let arg = get_args();

    match arg.action {
        Action::Play => {
            if let Err(e) = socket.send_to(b"play", &server) {
                return Err(PomdError::SocketSendError(e));
            }
        }

        Action::Pause => {
            if let Err(e) = socket.send_to(b"pause", &server) {
                return Err(PomdError::SocketSendError(e));
            }
        }

        Action::Stop => {
            if let Err(e) = socket.send_to(b"stop", &server) {
                return Err(PomdError::SocketSendError(e));
            }
        }

        Action::Toggle => {
            if let Err(e) = socket.send_to(b"toggle", &server) {
                return Err(PomdError::SocketSendError(e));
            }
        }

        Action::Status => {
            if arg.json {
                if let Err(e) = socket.send_to(b"json", &server) {
                    return Err(PomdError::SocketSendError(e));
                }
            } else if let Err(e) = socket.send_to(b"status", &server) {
                return Err(PomdError::SocketSendError(e));
            }

            let mut buf = [0; 100];

            match socket.recv_from(&mut buf) {
                Ok((_amt, _src)) => {
                    let status = str::from_utf8(&buf).unwrap();
                    println!("{}", status);
                }
                Err(e) => println!("Couldn't receive status: {}", e),
            }
        }
    }

    Ok(())
}
