use structopt::StructOpt;

#[derive(StructOpt, Debug)]
#[structopt(name = "pomd", setting = structopt::clap::AppSettings::ColoredHelp)]
pub struct Opt {
    ///Pomodoro length in minutes.
    #[structopt(
        short = "p",
        long = "pomodoro",
        value_name = "POMODORO",
        default_value = "25"
    )]
    pub pom_len: u64,

    ///Break length in minutes.
    #[structopt(short = "b", long = "break", value_name = "BREAK", default_value = "5")]
    pub break_len: u64,

    ///Long break length in minutes.
    #[structopt(
        short = "B",
        long = "long-break",
        value_name = "LONG BREAK",
        default_value = "15"
    )]
    pub long_len: u64,

    ///Run the process in the foreground.
    #[structopt(short = "F", long = "foreground")]
    pub foreground: bool,

    ///Number of rounds before a long break.
    #[structopt(
        short = "r",
        long = "rounds",
        value_name = "ROUNDS",
        default_value = "4"
    )]
    pub rounds: u64,

    ///Enables notifications.
    #[structopt(short = "n", long = "notifications")]
    pub notif: bool,
}
