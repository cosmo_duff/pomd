mod args;
mod handlers;
mod pomd;

use crate::{args::Opt, handlers::handle_notifications, pomd::Pomd};
use common::error::PomdError;
use daemonize::Daemonize;
use env_logger::Builder;
use log::{error, info, LevelFilter};
use structopt::StructOpt;

type Result<T> = std::result::Result<T, PomdError>;

fn main() {
    // setup logging
    let mut builder = Builder::new();
    builder.filter_level(LevelFilter::Info);
    builder.parse_env("POMD_LOG");
    builder.init();
    let args = Opt::from_args();
    if let Err(e) = run(args) {
        error!("{}", e);
    }
}

fn run(args: Opt) -> Result<()> {
    let (mut pomd, receiver) = Pomd::try_from_args(&args)?;

    // setup the daemon process if not to run in the foreground
    if !args.foreground {
        let daemonize = Daemonize::new();

        match daemonize.start() {
            Ok(_) => info!("Daemonized pomd"),
            Err(e) => return Err(PomdError::DaemonizeError(e)),
        }
    }

    // launch the notification thread if it was set by the args
    if args.notif {
        handle_notifications(receiver);
    }

    // launch the server loop to check for incoming commands from pomc
    pomd.recv_loop()
}
