use crate::{args::Opt, handlers::handle_message};

use common::{create_socket, error::PomdError, socket_path};
use libpom::{Pomodoro, PomodoroBuilder, State};
use log::error;

use std::{
    os::unix::net::UnixDatagram,
    sync::mpsc::{channel, Receiver},
};

pub(crate) struct Pomd {
    socket: UnixDatagram,
    pomodoro: Pomodoro,
}

type Result<T> = std::result::Result<T, PomdError>;

impl Pomd {
    pub(crate) fn try_from_args(args: &Opt) -> Result<(Pomd, Receiver<State>)> {
        let (sender, receiver) = channel();
        // create settings for pomodoro object
        let pom = PomodoroBuilder::new()
            .pomodoro_length(args.pom_len)
            .break_length(args.break_len)
            .long_break_length(args.long_len)
            .rounds(args.rounds)
            .external_sender(sender)
            .build();

        let socket = match create_socket(&socket_path("pomd")) {
            Ok(s) => s,
            Err(e) => return Err(PomdError::SocketCreationError(e)),
        };

        let pomd = Pomd {
            socket,
            pomodoro: pom,
        };

        Ok((pomd, receiver))
    }

    pub(crate) fn recv_loop(&mut self) -> Result<()> {
        // create buffer to write messages to
        // the buffer is the size  of the longest possible message "status" or "toggle"
        let mut buf = [0; 6];

        loop {
            match self.socket.recv_from(&mut buf) {
                Ok((_amt, src)) => {
                    let client = match src.as_pathname() {
                        Some(cli) => cli,
                        None => {
                            error!("Failed to get sending socket address");
                            continue;
                        }
                    };

                    //let sock = socket.try_clone().expect("Failed to clone socket");
                    if let Err(e) = handle_message(&mut self.pomodoro, &buf, &self.socket, client) {
                        match e {
                            PomdError::PlayError(pe) => {
                                error!("Play command failed to execute: {}", pe)
                            }
                            PomdError::SocketSendError(se) => {
                                error!("Failed to send status back to pomc: {}", se)
                            }
                            _ => {}
                        }
                    };

                    // clear the buffer
                    buf = [0; 6]
                }
                Err(e) => error!("Couldn't receive a datagram: {}", e),
            }
        }
    }
}
