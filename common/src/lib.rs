pub mod error;

use std::{
    env, fs,
    fs::DirBuilder,
    os::unix::net::UnixDatagram,
    path::{Path, PathBuf},
};

pub fn socket_path(name: &str) -> PathBuf {
    let mut socket_path = PathBuf::new();
    // try and add the runtime dir if it is available
    if let Ok(p) = env::var("XDG_RUNTIME_DIR") {
        socket_path.push(p);
    }

    // if the runtime directory does not exist or the var was not set default to /tmp
    if !socket_path.exists() {
        socket_path.push("/tmp");
    }

    let app_path = format!("{0}/{0}.socket", name);
    socket_path.push(app_path);

    socket_path
}

pub fn create_socket(path: &Path) -> Result<UnixDatagram, std::io::Error> {
    // get path to the socket to create needed directories
    if let Some(dirs) = path.parent() {
        // create the daemon socket path
        DirBuilder::new().recursive(true).create(&dirs)?
    }

    if path.exists() {
        fs::remove_file(&path)?
    }

    // create socket for listening
    UnixDatagram::bind(&path)
}
