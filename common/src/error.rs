use std::{error::Error, fmt, fmt::Display};

use daemonize::DaemonizeError;
use libpom::PomError;

#[derive(Debug)]
#[non_exhaustive]
pub enum PomdError {
    PlayError(PomError),
    DaemonizeError(DaemonizeError),
    SocketCreationError(std::io::Error),
    SocketSendError(std::io::Error),
    StatusDeserializeFailed(serde_json::Error),
}

impl Display for PomdError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            PomdError::PlayError(ref e) => {
                write!(f, "The play command failed to execute: {}", e)
            }
            PomdError::SocketSendError(ref e) => {
                write!(f, "Failed to send to the socket: {}", e)
            }
            PomdError::DaemonizeError(ref e) => {
                write!(f, "Failed to daemonize: {}", e)
            }
            PomdError::StatusDeserializeFailed(e) => {
                write!(f, "Failed to deserialize the status into JSON: {}", e)
            }
            PomdError::SocketCreationError(e) => {
                write!(f, "Failed to create the Unix socket: {}", e)
            }
        }
    }
}

impl Error for PomdError {}
