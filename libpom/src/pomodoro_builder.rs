use crate::{settings::Settings, Pomodoro, State};
use std::{sync::mpsc::Sender, time::Duration};

#[derive(Debug, Clone)]
/// Builds a pomodoro.
pub struct PomodoroBuilder {
    settings: Settings,
    external_sender: Option<Sender<State>>,
}

impl PomodoroBuilder {
    /// Creates a Pomodoro Builder
    pub fn new() -> Self {
        PomodoroBuilder {
            settings: Settings::default(),
            external_sender: None,
        }
    }

    /// Sets the pomodoro length.
    pub fn pomodoro_length(&mut self, length: u64) -> &mut Self {
        self.settings.pomodoro_length = Duration::from_secs(length * 60);
        self
    }

    /// Sets the break length.
    pub fn break_length(&mut self, length: u64) -> &mut Self {
        self.settings.break_length = Duration::from_secs(length * 60);
        self
    }

    /// Sets the long break length.
    pub fn long_break_length(&mut self, length: u64) -> &mut Self {
        self.settings.long_break_length = Duration::from_secs(length * 60);
        self
    }

    /// Sets the number of pomodoros before a long break.
    pub fn rounds(&mut self, rounds: u64) -> &mut Self {
        self.settings.rounds = rounds;
        self
    }

    /// Takes the sending half of a std channel so the receiving end can be used by the consumer.
    pub fn external_sender(&mut self, external_sender: Sender<State>) -> &mut Self {
        self.external_sender = Some(external_sender);
        self
    }

    /// Creates a new pomodoro from the settings.
    pub fn build(&mut self) -> Pomodoro {
        let mut pom: Pomodoro = Pomodoro::from(self.settings);
        if self.external_sender.is_some() {
            pom.external_sender = self.external_sender.clone();
        }
        pom
    }
}

impl Default for PomodoroBuilder {
    fn default() -> Self {
        Self::new()
    }
}
