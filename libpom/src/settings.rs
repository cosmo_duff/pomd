use std::time::Duration;

#[derive(Debug, Copy, Clone)]
pub struct Settings {
    pub(crate) pomodoro_length: Duration,
    pub(crate) break_length: Duration,
    pub(crate) long_break_length: Duration,
    pub(crate) rounds: u64,
}

impl Default for Settings {
    fn default() -> Settings {
        Settings {
            pomodoro_length: Duration::from_secs(25 * 60),
            break_length: Duration::from_secs(5 * 60),
            long_break_length: Duration::from_secs(15 * 60),
            rounds: 4,
        }
    }
}
