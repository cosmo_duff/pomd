use std::error::Error;
use std::fmt;
use std::io;

/// Errors returned when executing a pomodoro.
#[derive(Debug)]
pub enum PomError {
    ThreadFailed(io::Error),
    AlreadyRunning,
    TimerPanic(&'static str),
}

impl fmt::Display for PomError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            PomError::ThreadFailed(ref e) => e.fmt(f),
            PomError::AlreadyRunning => write!(f, "The pomodoro is already running"),
            PomError::TimerPanic(e) => write!(f, "{}", e),
        }
    }
}

impl Error for PomError {
    fn cause(&self) -> Option<&dyn Error> {
        match *self {
            PomError::ThreadFailed(ref e) => Some(e),
            PomError::AlreadyRunning => None,
            PomError::TimerPanic(_e) => None,
        }
    }
}
